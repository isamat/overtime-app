## Overtime app
##Key requirement: company needs documentation that salaried employees did or didn't get overtime each week

x Post -> date:date relation:text
x User -> devise
x AdminUser -> STI

##Features:
- Approval workflow 
- SMS sending 
- Admin panel dashboard
- Email summary to managers
- Needs to be documented if employee did not log overtime

##UI:

x Boostrap -> UI
X Icons for forms
- Needs to be documented


