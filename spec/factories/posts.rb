FactoryBot.define do 
	factory :post do
		date { Date.today }
		rational { "post 1" }
		user
	end
		factory :second_post, class: "Post"  do
		date { Date.yesterday }
		rational { "post 2" }
		user
		
	end
end