FactoryBot.define do 
	sequence :email do |n|
		"test#{n}@memail.com"
	end
	factory :user do
		first_name { "Alex" }
		last_name { "Roberts" }
		email { generate :email }
		password { "qwerty" }
		password_confirmation { "qwerty" }
	end
	factory :second_user, class: "User" do
		first_name { "Ben" }
		last_name { "Smith" }
		email { generate :email }
		password { "qwerty" }
		password_confirmation { "qwerty" }
	end
	factory :admin_user, class: "AdminUser" do
		first_name { "Admin" }
		last_name { "Iskaziyev" }
		email { generate :email }
		password { "qwerty" }
		password_confirmation { "qwerty" }
	end
end