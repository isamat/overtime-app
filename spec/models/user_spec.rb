require 'rails_helper'

RSpec.describe User, type: :model do
  before do 
    @user = FactoryBot.create(:user)
  end
  describe "craetion" do
  	it "can be craeted" do 
  		expect(@user).to be_valid
  	end
  	it "can not be created without firstname, lastname " do
  		@user.last_name = nil
  		@user.first_name = nil
  		expect(@user).to_not be_valid
  	end
  end
  describe "custom name method" do
    it "has full name method first  last name" do 
      expect(@user.full_name).to eq("ROBERTS, ALEX")
    end
  end
end
