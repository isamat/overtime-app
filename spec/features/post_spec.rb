require 'rails_helper'

describe 'navigate' do
	before do 
			@user = FactoryBot.create(:user)
			login_as(@user, :scope => :user)
	end
	describe 'index' do

		before do 
			visit posts_path
		end

		it "can be reached successfully" do
			expect(page.status_code).to eq(200)
		end
		it "has a title of Posts" do
			expect(page).to have_content(/Posts/) 
		end
		it 'has title of posts' do
			post1 = FactoryBot.create(:post)
			post2 = FactoryBot.create(:second_post)
			
			visit posts_path
			expect(page).to have_content(/post 1|post 2/)
		end
	end
	describe "New" do 
		it 'has a link from home page' do 
			visit root_path 

			click_link("new_post_from_nav")
			expect(page.status_code).to eq(200)
		end
	end

	describe "creation" do
		before do 
			visit new_post_path 
		end
		it "has new form can be reached" do
			
			expect(page.status_code).to eq(200)
		end
		it "can be created from new form" do

			fill_in 'post[date]', with: Date.today
			fill_in 'post[rational]', with: 'rationale'
			click_on 'Save'
			expect(page).to have_content("rationale")
		end
		it "will have user associated with it" do 
			fill_in 'post[date]', with: Date.today
			fill_in 'post[rational]', with: 'User Association'
			click_on 'Save'

			expect(User.last.posts.last.rational).to eq("User Association")
		end
	end
	describe "edit" do
		before do
			@post = FactoryBot.create(:post)
		end
		it 'can be reached by clicking edit on index page' do 
			visit posts_path

			click_link("edit_#{@post.id}")
			expect(page.status_code).to eq(200)
		end
		it 'can be edited' do
			visit edit_post_path(@post)

			fill_in 'post[date]', with: Date.today
			fill_in 'post[rational]', with: 'Edited raional'
			click_on 'Save'

			expect(page).to have_content("Edited raional")
		end
	end
	describe "Delete" do 
		it 'can be deleted' do
			@post = FactoryBot.create(:post)
			visit posts_path
			click_link("delete_post_#{@post.id}_from_index")

			expect(page.status_code).to eq(200)
		end
	end
end